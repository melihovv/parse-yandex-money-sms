<?php

declare(strict_types = 1);

/**
 * Возвращает номер кошелька, сумму перевода и пароль из смс от Яндекс.Деньги.
 *
 * @param string $sms
 * @return array
 * @throws InvalidArgumentException
 */
function parse_yandex_money_sms(string $sms)
{
    $patterns = [
        // Номер кошелька - последовательность не менее 11 цифр, начинающихся на 41001.
        '/\b(41001\d{6}\d*)\b/m' => 'purse_number',

        // Сумма перевода - дробное или целое число с последующим буквенным обозначением валюты.
        '/\b([1-9]\d*(?:[.,]\d+)?)[a-zA-Zа-яА-Я]+\b/uim' => 'sum',
        // Сумма перевода - просто дробное число без валюты.
        '/\b([1-9]\d*[.,]\d+)\b/m' => 'sum',

        // Пароль - последовательность чисел.
        '/\b(\d+)\b/m' => 'password',
    ];

    $data = [];

    foreach ($patterns as $pattern => $field) {
        if (! isset($data[$field]) && preg_match($pattern, $sms, $matches) && isset($matches[1])) {
            $data[$field] = $matches[1];
            $sms = str_replace($matches[1], '', $sms);
        }
    }

    $diff = array_diff(array_unique(array_values($patterns)), array_keys($data));

    if ($diff) {
        throw new InvalidArgumentException(sprintf(
            'Cannot parse %s from sms [%s]',
            implode(', ', $diff),
            $sms
        ));
    }

    return $data;
}

$sms = '
Пароль: 0
Спишется 4р.
Перевод на счет 4100119511193
';

$data = parse_yandex_money_sms($sms);

var_dump($data);
